﻿using System;

namespace Triangles
{
    class Program
    {
        //5.	Да се състави програма, която определя
        //дали три числа могат да бъдат страни на триъгълник,
        //ако да - да се изведе периметъра и лицето на този триъгълник,
        //в противен случай да се изведе съобщение за грешка.
        static void Main(string[] args)
        {
            Console.WriteLine("Enter the 3 sides of a triangle.");
            Console.Write("Side 1: ");
            double side1 = double.Parse(Console.ReadLine());

            Console.Write("Side 2: ");
            double side2 = double.Parse(Console.ReadLine());

            Console.Write("Side 3: ");
            double side3 = double.Parse(Console.ReadLine());

            if (side1 + side2 > side3
                && side2 + side3 > side1
                && side1 + side3 > side2)
            {
                double perimeter = side1 + side2 + side3;
                Console.WriteLine("Perimeter: " + perimeter);

                double p = perimeter / 2;
                double s = Math.Sqrt(p * (p - side1) * (p - side2) * (p - side3));
                Console.WriteLine("Surface: " + s);
            }
            else
            {
                Console.WriteLine("This triangle does not exist.");
            }
        }
    }
}
