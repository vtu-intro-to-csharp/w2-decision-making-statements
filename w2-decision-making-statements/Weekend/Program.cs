﻿using System;

namespace Weekend
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.Write("Enter a number for a day of the week: ");
            int dayOfWeek = int.Parse(Console.ReadLine());

            switch (dayOfWeek)
            {
                case 1:
                case 2:
                case 3:
                case 4:
                case 5: Console.WriteLine("Working day"); break;
                case 6:
                case 7: Console.WriteLine("Weekend"); break;
                default: Console.WriteLine("Not a valid number"); break;
            }
        }
    }
}
