﻿using System;

namespace MaxOfTwo
{
    class Program
    {
        //1.	Да се състави програма,
        //която въвежда две числа,
        //намира и извежда по-голямото от тях.
        static void Main(string[] args)
        {
            Console.WriteLine("Enter two integers.");
            Console.Write("First number: ");
            int number1 = int.Parse(Console.ReadLine());

            Console.Write("Second number: ");
            int number2 = int.Parse(Console.ReadLine());

            if (number1 > number2)
            {
                Console.WriteLine("The larger is: " + number1);
            }
            else if (number1 < number2)
            {
                Console.WriteLine("The larger is: " + number2);
            }
            else
            {
                Console.WriteLine("The numbers are equal.");
            }
        }
    }
}
