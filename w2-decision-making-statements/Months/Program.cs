﻿using System;

namespace Months
{
    class Program
    {
        // Да се състави програма, която по въведен номер на месец
        // извежда името на месеца и броя на дните в него.
        // За броя на дните на м. февруари, да се въведе
        // допълнително и годината.
        static void Main(string[] args)
        {
            Console.Write("Enter a number for a month (1-12): ");
            int month = int.Parse(Console.ReadLine());

            bool isLeap = false;
            if (month == 2)
            {
                Console.Write("Enter a year: ");
                int year = int.Parse(Console.ReadLine());
                isLeap = year % 4 == 0 && year % 100 != 0 || year % 400 == 0;
            }

            switch (month)
            {
                case 1: Console.WriteLine("Jan - 31"); break;
                case 2: Console.WriteLine("Feb - " + (isLeap ? 29 : 28)); break;
                case 3: Console.WriteLine("March - 31"); break;
                case 4: Console.WriteLine("Apr - 30"); break;
                case 5: Console.WriteLine("May - 31"); break;
                case 6: Console.WriteLine("June - 30"); break;
                case 7: Console.WriteLine("July - 31"); break;
                case 8: Console.WriteLine("Aug - 31"); break;
                case 9: Console.WriteLine("Sep - 30"); break;
                case 10: Console.WriteLine("Oct - 31"); break;
                case 11: Console.WriteLine("Nov - 30"); break;
                case 12: Console.WriteLine("Dec - 31"); break;
                default: Console.WriteLine("Not a valid number."); break;
            }
        }
    }
}
