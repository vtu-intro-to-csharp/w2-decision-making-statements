﻿using System;

namespace LeapYear
{
    class Program
    {
        //Да се състави програма, която установява
        //дали дадена година е високосна.
        //(Една година е високосна, ако се дели на 4 и не се дели на 100
        //или ако се дели на 400.)
        static void Main(string[] args)
        {
            Console.Write("Enter a year: ");
            int year = int.Parse(Console.ReadLine());

            if (year % 4 == 0 && year % 100 != 0 || year % 400 == 0)
            {
                Console.WriteLine("The year is leap.");
            }
            else
            {
                Console.WriteLine("The year is not leap.");
            }
        }
    }
}
